FROM php:7.1-apache

# install libraries
 RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y \
       libjpeg-dev \
       libpng12-dev \
       git \
       zip \
    ; \
    rm -rf /var/lib/apt/lists/*;

# install and configure PHP extensions
RUN set -ex; \
   docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
   docker-php-ext-install gd mysqli opcache

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
     echo 'opcache.memory_consumption=128'; \
     echo 'opcache.interned_strings_buffer=8'; \
     echo 'opcache.max_accelerated_files=4000'; \
     echo 'opcache.revalidate_freq=2'; \
     echo 'opcache.fast_shutdown=1'; \
     echo 'opcache.enable_cli=1'; \
  } > /usr/local/etc/php/conf.d/opcache-recommended.ini

# enable apache mod rewrite
RUN a2enmod rewrite

#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer;

WORKDIR /var/www/html
